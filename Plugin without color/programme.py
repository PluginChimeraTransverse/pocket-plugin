import os
import chimera
import shutil


def main():
	
	fichier = os.path.abspath(os.path.realpath(__file__))
	dossier_src = os.path.dirname(fichier)					 # chemin absolu de pluginPocket/
	dossier_tempo = os.path.dirname(dossier_src)
	os.chdir(dossier_tempo)
	#---------------------------------------------------------------------------------------------
	chimera.runCommand("write 0 tmp.pdb") 			#sauvegarde le fichier
	chimera.runCommand("close 0") 					#ferme le fichier ouvert de base

	os.system("fpocket -f tmp.pdb") 				#genere un dossier avec les poches
	os.chdir(dossier_tempo + "/tmp_out/")			#on se place dans ce dossier

	chimera.runCommand("open tmp_out.pdb")			#ouverture du fichier avec les poches
	
	os.chdir(dossier_tempo)		 					#on se replace dans le dossier 'tempo/'
	shutil.rmtree(dossier_tempo + "/tmp_out/")		#supprime le dossier tmp_out/						
	os.remove("tmp.pdb")							#supprime le fichier tmp.pdb
	
