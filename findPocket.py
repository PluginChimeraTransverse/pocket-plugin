import os
import chimera
import shutil
#---------------------------------------------------------------------------------------------
file = "file"
path = "/home/murat/Bureau/plugin/"
#---------------------------------------------------------------------------------------------
chimera.runCommand("write 0 "+file+".pdb") 	#sauvegarde le fichier
chimera.runCommand("close 0") 				#ferme le fichier ouvert de base

os.chdir(path) 								#se place dans le repertoire
os.system("fpocket -f "+file+".pdb") 		#genere un dossier
os.chdir(path+file+"_out")					#on se place dans ce dossier

chimera.runCommand("open "+file+"_out.pdb")	#ouverture du fichier avec les poches
shutil.rmtree(path+file+"_out")				#supprime le dossier 

