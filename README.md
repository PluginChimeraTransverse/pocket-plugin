# Pocket plugin

## DONE

- Save the molecule opened with chimera as pdb
- Close the molecule
- Perform fpocket on it and creates a directory
- Access this directory
- Open the file containing the pockets in Chimera
- Delete the directory created


## TO DO

- Use the python script as a plugin
- Have a button to click to perform it
- Color the pockets

