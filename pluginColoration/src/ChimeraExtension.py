import chimera.extension


class FindPocketsEMO(chimera.extension.EMO):
	def name(self):
		return 'findPockets' #'ProBiS'
	def description(self):
		return 'pockets visualization' #'protein binding site detection'
	def categories(self):
		return ['Structure Analysis'] #['Surface/Binding Analysis']
	def icon(self):
		return self.path('klika.ico')
	def activate(self):
		self.module('programme').main()
		return None

chimera.extension.manager.registerExtension(FindPocketsEMO(__file__))
