import os
import chimera
import shutil
from Tkinter import *


def chooseArea(colors,lettre,USED_CHAIN):
	window = Tk()

	window.title("Fpocket")

	window.geometry('350x200')

	lbl = Label(window, text="Choose the area to color (in A)")

	lbl.grid(column=0, row=0)

	txt = Entry(window,width=10)

	txt.grid(column=1, row=0)

	def clicked():

		area = txt.get()

		chimera.runCommand('color tan')
		colorPockets(colors, lettre, area)

		commande = "surface #0:"+",".join(USED_CHAIN)
		chimera.runCommand(commande)


	btn = Button(window, text="OK", command=clicked)

	btn.grid(column=2, row=0)

	window.mainloop()


def getListOfFiles(directory):
	return os.listdir(directory)


def getListOfPocketFiles(directory):
	''' retourne la liste des fichiers de poche en format pdb contenu dans le dossier '''
	file_list = getListOfFiles(directory)
	pdb_list = []
	for e in file_list:
		if(e[:6] == "pocket" and e[-8:]=="_atm.pdb"):
			pdb_list.append(e)
	return pdb_list

def strList(liste):
	res = ""
	for e in liste:
		res += e +"\n"
	return res


def ajouteSousDossier(chemin, sous_dossier):
	if(chemin.endswith('/')==False):
		chemin += "/"
	return chemin + sous_dossier

def supprimeFichiersDansDossier(chemin):
	os.chdir(chemin)
	liste_fichiers = getListOfFiles(chemin)
	for fichier in liste_fichiers:
		if os.path.isdir(chemin+fichier):
			shutil.rmtree(chemin+fichier)
		else: #fichier
			os.remove(fichier)

def supprimeCaracteres(list, char):
	''' renvoie la meme list mais en supprimant les elements char '''
	newListe = []
	for e in list:
		if(e != char):
			newListe.append(e)
	return newListe

def createListOfColors(size):
	'''genere une liste de taille size qui contient differentes couleurs '''
	maxi = int("eeeeee",16)
	mini = int("333333",16)
	inter = (maxi-mini)//(size-1)
	liste = []
	for i in range(size):
		hexValue = hex(mini+i*inter)
		color = str(hexValue)
		if(len(color)>8):
			liste.append("#00ffff")
		else:
			liste.append("#"+color[2:])
	return liste

def colorPockets(colors, lettre, rayon):
	''' colore de la couleur en parametre les atomes contenues dans le fichier '''

	for i in range(len(colors)): 
		command = "color "+colors[i]+" #0:"+str(i+1)+"."+lettre+" za<"+str(rayon) #color 0.5,0.5,0.5 #0:21.C za<5 
		print("command : "+command)
		chimera.runCommand(command)

def main():
	fichier = os.path.abspath(os.path.realpath(__file__)) #chemin absolu de programme.py

	dir_src = os.path.dirname(fichier) # chemin absolu de pluginPocket/
	dir_tempo = ajouteSousDossier(os.path.dirname(dir_src), "tempo/")
	dir_tmp_out = ajouteSousDossier(dir_tempo, "tmp_out/")
	dir_pockets = ajouteSousDossier(dir_tmp_out, "pockets/")

	supprimeFichiersDansDossier(dir_tempo)
	#---------------------------------------------------------------------------------------------
	chimera.runCommand("write 0 "+dir_tempo+"tmp.pdb") 			#sauvegarde le fichier ouvert de base
	chimera.runCommand("close 0") 								#ferme le fichier ouvert de base
	os.system("fpocket -f "+dir_tempo+"tmp.pdb") 				#genere un dossier avec les poches
	#----------- modification de la chaine d'appartenance des poches -----------------------------
	USED_CHAIN = []
	file = open(dir_tmp_out+"tmp_out.pdb","r")
	for line in file:
		if(line[:4]=="ATOM"):
			chaine = "."+line[21]
			if(chaine not in USED_CHAIN):
				USED_CHAIN.append(chaine)
	file.close()
	#-------------------------------------------------
	lettre = 'p'
	file = open(dir_tmp_out+"tmp_out.pdb","r")
	file_new = open(dir_tmp_out+"tmp_out_new.pdb","w")
	for line in file:
		if(line[:6]=="HETATM"):
			file_new.write(line[:21]+lettre+line[22:])
		else:
			file_new.write(line)
	file.close();
	file_new.close();
	#----------- ouverture du nouveau fichier-----------------------------------------------------
	chimera.runCommand("open "+dir_tmp_out+"tmp_out_new.pdb")       
	#----------- coloration des poches -----------------------------------------------------------
	pockets = getListOfPocketFiles(dir_pockets)
	colors = createListOfColors(len(pockets))

	chooseArea(colors,lettre,USED_CHAIN)
	'''
	colorPockets(colors, lettre, area)

	commande = "surface #0:"+",".join(USED_CHAIN)


	chimera.runCommand(commande)'''
	



	
	
