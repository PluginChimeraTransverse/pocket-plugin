Pocket 1 :
	Score : 			38.976
	Druggability Score : 		0.806
	Number of Alpha Spheres : 	58
	Total SASA : 			434.367
	Polar SASA : 			174.727
	Apolar SASA : 			259.640
	Volume : 			1212.899
	Mean local hydrophobic density : 24.308
	Mean alpha sphere radius :	4.234
	Mean alp. sph. solvent access :  0.501
	Apolar alpha sphere proportion : 0.448
	Hydrophobicity score:		31.529
	Volume score: 			 4.824
	Polarity score:			 12
	Charge score :			 2
	Proportion of polar atoms: 	31.250
	Alpha sphere density : 		5.199
	Cent. of mass - Alpha Sphere max dist: 14.485
	Flexibility : 			 0.358

Pocket 2 :
	Score : 			38.651
	Druggability Score : 		0.274
	Number of Alpha Spheres : 	64
	Total SASA : 			324.075
	Polar SASA : 			159.837
	Apolar SASA : 			164.237
	Volume : 			644.461
	Mean local hydrophobic density : 21.000
	Mean alpha sphere radius :	4.200
	Mean alp. sph. solvent access :  0.553
	Apolar alpha sphere proportion : 0.344
	Hydrophobicity score:		18.846
	Volume score: 			 3.538
	Polarity score:			 8
	Charge score :			 2
	Proportion of polar atoms: 	44.444
	Alpha sphere density : 		3.890
	Cent. of mass - Alpha Sphere max dist: 9.501
	Flexibility : 			 0.354

Pocket 3 :
	Score : 			36.317
	Druggability Score : 		0.073
	Number of Alpha Spheres : 	65
	Total SASA : 			239.164
	Polar SASA : 			123.727
	Apolar SASA : 			115.437
	Volume : 			386.041
	Mean local hydrophobic density : 7.500
	Mean alpha sphere radius :	3.708
	Mean alp. sph. solvent access :  0.497
	Apolar alpha sphere proportion : 0.185
	Hydrophobicity score:		24.000
	Volume score: 			 4.444
	Polarity score:			 12
	Charge score :			 -3
	Proportion of polar atoms: 	42.105
	Alpha sphere density : 		4.659
	Cent. of mass - Alpha Sphere max dist: 10.473
	Flexibility : 			 0.193

Pocket 4 :
	Score : 			29.814
	Druggability Score : 		0.277
	Number of Alpha Spheres : 	45
	Total SASA : 			243.051
	Polar SASA : 			123.496
	Apolar SASA : 			119.555
	Volume : 			627.432
	Mean local hydrophobic density : 24.000
	Mean alpha sphere radius :	4.206
	Mean alp. sph. solvent access :  0.591
	Apolar alpha sphere proportion : 0.556
	Hydrophobicity score:		9.571
	Volume score: 			 3.857
	Polarity score:			 6
	Charge score :			 1
	Proportion of polar atoms: 	37.500
	Alpha sphere density : 		3.046
	Cent. of mass - Alpha Sphere max dist: 8.236
	Flexibility : 			 0.309

Pocket 5 :
	Score : 			26.050
	Druggability Score : 		0.025
	Number of Alpha Spheres : 	44
	Total SASA : 			245.404
	Polar SASA : 			147.587
	Apolar SASA : 			97.818
	Volume : 			657.569
	Mean local hydrophobic density : 12.000
	Mean alpha sphere radius :	4.377
	Mean alp. sph. solvent access :  0.685
	Apolar alpha sphere proportion : 0.295
	Hydrophobicity score:		14.500
	Volume score: 			 4.417
	Polarity score:			 7
	Charge score :			 2
	Proportion of polar atoms: 	53.846
	Alpha sphere density : 		2.917
	Cent. of mass - Alpha Sphere max dist: 7.823
	Flexibility : 			 0.480

Pocket 6 :
	Score : 			20.648
	Druggability Score : 		0.013
	Number of Alpha Spheres : 	39
	Total SASA : 			187.415
	Polar SASA : 			122.203
	Apolar SASA : 			65.212
	Volume : 			355.514
	Mean local hydrophobic density : 9.000
	Mean alpha sphere radius :	4.311
	Mean alp. sph. solvent access :  0.574
	Apolar alpha sphere proportion : 0.256
	Hydrophobicity score:		15.200
	Volume score: 			 3.500
	Polarity score:			 5
	Charge score :			 0
	Proportion of polar atoms: 	40.000
	Alpha sphere density : 		2.656
	Cent. of mass - Alpha Sphere max dist: 6.410
	Flexibility : 			 0.321

Pocket 7 :
	Score : 			20.361
	Druggability Score : 		0.022
	Number of Alpha Spheres : 	36
	Total SASA : 			241.507
	Polar SASA : 			102.630
	Apolar SASA : 			138.877
	Volume : 			619.857
	Mean local hydrophobic density : 11.077
	Mean alpha sphere radius :	4.080
	Mean alp. sph. solvent access :  0.484
	Apolar alpha sphere proportion : 0.361
	Hydrophobicity score:		7.091
	Volume score: 			 3.636
	Polarity score:			 8
	Charge score :			 2
	Proportion of polar atoms: 	43.478
	Alpha sphere density : 		4.085
	Cent. of mass - Alpha Sphere max dist: 10.844
	Flexibility : 			 0.265

Pocket 8 :
	Score : 			19.365
	Druggability Score : 		0.048
	Number of Alpha Spheres : 	45
	Total SASA : 			429.342
	Polar SASA : 			264.234
	Apolar SASA : 			165.108
	Volume : 			892.725
	Mean local hydrophobic density : 1.600
	Mean alpha sphere radius :	4.623
	Mean alp. sph. solvent access :  0.590
	Apolar alpha sphere proportion : 0.111
	Hydrophobicity score:		33.727
	Volume score: 			 4.636
	Polarity score:			 8
	Charge score :			 2
	Proportion of polar atoms: 	50.000
	Alpha sphere density : 		4.874
	Cent. of mass - Alpha Sphere max dist: 13.381
	Flexibility : 			 0.450

Pocket 9 :
	Score : 			16.450
	Druggability Score : 		0.130
	Number of Alpha Spheres : 	38
	Total SASA : 			288.034
	Polar SASA : 			167.271
	Apolar SASA : 			120.763
	Volume : 			695.839
	Mean local hydrophobic density : 14.000
	Mean alpha sphere radius :	4.488
	Mean alp. sph. solvent access :  0.635
	Apolar alpha sphere proportion : 0.395
	Hydrophobicity score:		45.583
	Volume score: 			 4.000
	Polarity score:			 4
	Charge score :			 1
	Proportion of polar atoms: 	48.000
	Alpha sphere density : 		4.792
	Cent. of mass - Alpha Sphere max dist: 10.930
	Flexibility : 			 0.327

